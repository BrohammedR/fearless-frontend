import React, { useEffect, useState } from 'react';

function ConferenceForm(props) {
	const [locations, setLocations] = useState([]);
	const [name, setName] = useState('');
	const [starts, setStarts] = useState('');
	const [ends, setEnd] = useState('');
	const [MaxPresentations, setMaxPresentations] = useState(0);
	const [MaxAttendees, setMaxAttendees] = useState(0);
	const [location, setLocation] = useState('');

	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {};
		data.name = name;
		data.starts = starts;
		data.ends = ends;
		data.location = location;
		data.max_presentations = MaxPresentations;
		data.max_attendees = MaxAttendees;

		const conferenceUrl = 'http://localhost:8000/api/conferences/';
		const fetchConfig = {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		};

		const response = await fetch(conferenceUrl, fetchConfig);
		if (response.ok) {
			const newConference = await response.json();
			console.log(newConference);

			setName('');
			setStarts('');
			setEnd('');
			setLocation('');
			setMaxPresentations('');
			setMaxAttendees('');
		}
	};

	const handleNameChange = (event) => {
		const value = event.target.value;
		setName(value);
	};

	const handleMaxPresentations = (event) => {
		const value = event.target.value;
		setMaxPresentations(value);
	};

	const handleMaxAttendees = (event) => {
		const value = event.target.value;
		setMaxAttendees(value);
	};

	const handleStartsChange = (event) => {
		const value = event.target.value;
		setStarts(value);
	};

	const handleEndChange = (event) => {
		const value = event.target.value;
		setEnd(value);
	};

	const handleLocationChange = (event) => {
		const value = event.target.value;
		setLocation(value);
	};

	const fetchData = async () => {
		const url = 'http://localhost:8000/api/locations/';

		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			setLocations(data.locations);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Create a new conference</h1>
					<form onSubmit={handleSubmit} id="create-conference-form">
						<div className="form-floating mb-3">
							<input
								onChange={handleNameChange}
								placeholder="Name"
								required
								type="text"
								name="name"
								value={name}
								id="name"
								className="form-control"
							/>
							<label htmlFor="name">Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleStartsChange}
								placeholder="Starts"
								required
								type="datetime-local"
								name="starts"
								value={starts}
								id="starts"
								className="form-control"
							/>
							<label htmlFor="starts">Starts</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleEndChange}
								placeholder="Ends"
								required
								type="datetime-local"
								name="ends"
								value={ends}
								id="ends"
								className="form-control"
							/>
							<label htmlFor="ends">Ends</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleMaxPresentations}
								placeholder="Max Presentations"
								required
								type="number"
								name="Max Presentations"
								value={MaxPresentations}
								id="MaxPresentations"
								className="form-control"
							/>
							<label htmlFor="Max Presentations">Max Presentations</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleMaxAttendees}
								placeholder="Max Attendees"
								required
								type="number"
								name="Max Attendees"
								value={MaxAttendees}
								id="MaxAttendees"
								className="form-control"
							/>
							<label htmlFor="Max Attendees">Max Attendees</label>
						</div>
						<div className="mb-3">
							<select
								required
								name="location"
								id="location"
								value={location}
								className="form-select"
								onChange={handleLocationChange}
							>
								<option value="">Choose a location</option>
								{locations.map((location) => {
									return (
										<option key={location.id} value={location.id}>
											{location.name}
										</option>
									);
								})}
							</select>
						</div>
						<button type="submit" className="btn btn-primary">
							Create Conference
						</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default ConferenceForm;
