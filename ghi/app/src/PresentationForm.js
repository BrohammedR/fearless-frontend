import React, { useEffect, useState } from 'react';

function PresentationForm() {
	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [title, setTitle] = useState('');
	const [synopsis, setSynopsis] = useState('');
	const [company, setCompany] = useState('');
	const [conferences, setConferences] = useState([]);
	const [conference, setConference] = useState('');

	const handlePresenterName = (event) => {
		const value = event.target.value;
		setName(value);
	};

	const handleEmail = (event) => {
		const value = event.target.value;
		setEmail(value);
	};

	const handleTitle = (event) => {
		const value = event.target.value;
		setTitle(value);
	};

	const handleSynopsis = (event) => {
		const value = event.target.value;
		setSynopsis(value);
	};

	const handleCompany = (event) => {
		const value = event.target.value;
		setCompany(value);
	};

	const handleConference = (event) => {
		const value = event.target.value;
		const selectedConference = conferences.find(
			(conference) => conference.name === value
		);
		setConference(selectedConference);
	};

	const handleSubmit = async (event) => {
		event.preventDefault();
		const data = {};
		data.synopsis = synopsis;
		data.presenter_name = name;
		data.presenter_email = email;
		data.company_name = company;
		data.conference = conference.name;
		data.title = title;
		const presentationUrl = `http://localhost:8000${conference.href}presentations/`;
		const fetchConfig = {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		};
		const response = await fetch(presentationUrl, fetchConfig);
		if (response.ok) {
			const newLocation = await response.json();
			console.log(newLocation);
			setName('');
			setTitle('');
			setEmail('');
			setSynopsis('');
			setCompany('');
			setConference('');
			event.target.reset();
		}
	};

	const fetchData = async () => {
		const url = 'http://localhost:8000/api/conferences/';

		const response = await fetch(url);

		if (response.ok) {
			const data = await response.json();
			// console.log(data.conferences)
			setConferences(data.conferences);
		}
	};

	useEffect(() => {
		fetchData();
	}, []);

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Create a new presentation</h1>
					<form onSubmit={handleSubmit} id="create-presentation-form">
						<div className="mb-3">
							<select
								onChange={handleConference}
								required
								id="conference"
								name="href"
								className="form-select"
							>
								<option value="">Choose a Conference</option>
								{conferences.map((conference) => {
									return (
										<option key={conference.href} value={conference.name}>
											{conference.name}
										</option>
									);
								})}
							</select>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handlePresenterName}
								placeholder="Presenter Name"
								required
								type="text"
								name="presenter_name"
								id="presenter_name"
								className="form-control"
							/>
							<label htmlFor="presenter_name">Presenter Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleEmail}
								placeholder="Presenter Email"
								required
								type="email"
								name="presenter_email"
								id="presenter_email"
								className="form-control"
							/>
							<label htmlFor="presenter_email">Presenter Email</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleCompany}
								placeholder="Company Name"
								type="text"
								id="company_name"
								name="company_name"
								className="form-control"
							/>
							<label htmlFor="company_name">Company Name</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleTitle}
								placeholder="Title your presentation"
								required
								type="text"
								name="title"
								id="title"
								className="form-control"
							/>
							<label htmlFor="title">Title your presentation</label>
						</div>
						<div className="row">
							<div className="form-floating mb-3">
								<textarea
									onChange={handleSynopsis}
									placeholder="Synopsis"
									required
									type="text"
									name="synopsis"
									id="synopsis"
									className="form-control"
									rows="5"
									style={{ height: '150px' }}
								></textarea>
								<label htmlFor="synopsis" className="form-label">
									Synopsis
								</label>
							</div>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	);
}

export default PresentationForm;
